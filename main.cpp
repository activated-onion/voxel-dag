#include <functional>
#include <iostream>
#include <memory>
#include <stdint.h>
#include <vector>

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

using namespace glm;
using namespace std;

struct SdlRaii {
	SDL_Window * window;
	SDL_Renderer * renderer;
	SDL_Texture * texture;
	
	SdlRaii (int width, int height);
	~SdlRaii ();
	
	SdlRaii (const SdlRaii & o) = delete;
	SdlRaii (const SdlRaii && o) = delete;
	SdlRaii & operator = (const SdlRaii & o) = delete;
	
	void swap_buffers () const;
};

SdlRaii::SdlRaii (int w, int h) {
	SDL_Init (SDL_INIT_VIDEO);
	
	SDL_CreateWindowAndRenderer (w, h, 0, &window, &renderer);
	
	texture = SDL_CreateTexture (
		renderer, 
		SDL_PIXELFORMAT_ARGB8888,
		SDL_TEXTUREACCESS_STREAMING,
		w, h
	);
}

SdlRaii::~SdlRaii () {
	SDL_DestroyTexture (texture);
	SDL_DestroyRenderer (renderer);
	SDL_DestroyWindow (window);
	
	SDL_Quit ();
}

int pixel_index (int x, int y) {
	return y * 800 + x;
}

static const float pi = 3.1415926535f;
static const int max_level = 8;

int res_at_level (int level) {
	return 1 << level;
}

int cube_size_at_level (int level) {
	return 1 << (max_level - level);
}

// Z is up, Y is forward, X is right
int voxel_index (int x, int y, int z, int level) {
	const int res = res_at_level (level);
	return z * res * res + y * res + x;
}

int voxel_index (ivec3 v, int level) {
	return voxel_index (v.x, v.y, v.z, level);
}

typedef uint64_t DagLeaf;
struct FullSvoNode {
	uint8_t childmask;
	uint64_t offset_ptr [8];
};

DagLeaf make_leaf (const vector <bool> & voxels, ivec3 v) {
	DagLeaf result = 0;
	
	for (int z = 0; z < 4; z++) {
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				if (voxels [voxel_index (v.x + x, v.y + y, v.z + z, max_level)]) {
					result |= ((uint64_t)1 << (z * 4 * 4 + y * 4 + x));
				}
			}
		}
	}
	
	return result;
}

vector <DagLeaf> make_leaves (const vector <bool> & voxels) {
	vector <DagLeaf> result;
	result.resize (voxels.size () / 4 / 4 / 4);
	
	const int quarter_res = res_at_level (max_level) / 4;
	
	for (int z = 0; z < quarter_res; z++) {
		for (int y = 0; y < quarter_res; y++) {
			for (int x = 0; x < quarter_res; x++) {
				result [z * quarter_res * quarter_res + y * quarter_res + x] = make_leaf (voxels, ivec3 (4) * ivec3 (x, y, z));
			}
		}
	}
	
	return result;
}

vector <FullSvoNode> make_inner_node (const vector <DagLeaf> & leaves) {
	vector <FullSvoNode> result;
	result.resize (leaves.size () / 8);
	
	for (int i = 0; i < (int)result.size (); i++) {
		
	}
	
	return result;
}

void make_voxels (vector <bool> & voxels) {
	const int res = res_at_level (max_level);
	const float res_f = (float)res;
	
	for (int z = 0; z < res; z++) {
		for (int y = 0; y < res; y++) {
			for (int x = 0; x < res; x++) {
				const float xf = x / res_f;
				const float yf = y / res_f;
				const float zf = z / res_f;
				
				const float amp = 
					sin (xf * 2.5f * pi) +
					sin (yf * 3.0f * pi);
				
				if (zf - 0.5f < amp * 0.125f) {
					voxels [voxel_index (x, y, z, max_level)] = true;
				}
			}
		}
	}
}

void render_voxels_xz (const vector <bool> & voxels, uint8_t * pixels) {
	for (int y = 0; y < 480; y++) {
		for (int x = 0; x < 800; x++) {
			const int pixel_i = pixel_index (x, y) * 4;
			
			const int vx = x - 400 + 128;
			const int vz = -y + 240 + 128;
			
			if (vx < 0 || vx >= 256 || vz < 0 || vz >= 256) {
				pixels [pixel_i + 0] = 58;
				pixels [pixel_i + 1] = 58;
				pixels [pixel_i + 2] = 58;
			}
			else {
				for (int i = 0; i < 256; i++) {
					const int vy = i;
					
					const int voxel_i = voxel_index (vx, vy, vz, max_level);
					
					if (voxels [voxel_i]) {
						pixels [pixel_i + 0] = 220;
						pixels [pixel_i + 1] = 220;
						pixels [pixel_i + 2] = 220;
						goto hit_voxel;
					}
				}
				
				// missed
				
				pixels [pixel_i + 0] = 58;
				pixels [pixel_i + 1] = 58;
				pixels [pixel_i + 2] = 58;
				
				hit_voxel:
				;
			}
		}
	}
}

struct TraceResult {
	int x, y, z;
	float t;
	bool hit;
};

TraceResult raytrace (const vector <DagLeaf> & leaves, const vec3 & start, const vec3 & dir) {
	const int quarter_res = res_at_level (max_level - 2);
	
	const float res_f = (float)res_at_level (max_level);
	
	for (float x = 0.0f; x < res_f; x++) {
		float t = 0.0f;
		if (dir.x != 0.0f) {
			t = (x - start.x) / dir.x;
		}
		
		if (t > 0.0f) {
			const float y = start.y + t * dir.y;
			const float z = start.z + t * dir.z;
			
			if (y >= 0.0f && y < res_f && z >= 0.0f && z < res_f) {
				const int xi = (int)x;
				const int yi = (int)y;
				const int zi = (int)z;
				
				const int xl = xi / 4;
				const int yl = yi / 4;
				const int zl = zi / 4;
				
				const int xr = xi & 3;
				const int yr = yi & 3;
				const int zr = zi & 3;
				
				if ((leaves [zl * quarter_res * quarter_res + yl * quarter_res + xl] & ((uint64_t)1 << (zr * 4 * 4 + yr * 4 + xr))) != 0) {
					return TraceResult {
						xi, yi, zi,
						t, true
					};
				}
			}
		}
	}
	
	return TraceResult {
		0, 0, 0,
		0.0f,
		false
	};
}

void render_voxels_ortho (const vector <DagLeaf> & voxels, uint8_t * pixels) {
	const vec3 dir = normalize (vec3 (1, 1, -1));
	const vec3 camera_pos = -dir * 512.0f + vec3 (128.0f, 128.0f, 128.0f);
	const vec3 camera_right = normalize (cross (dir, vec3 (0, 0, 1)));
	const vec3 camera_up = cross (dir, camera_right);
	
	const float scale = 256.0f / 512.0f;
	
	for (int y = 0; y < 480; y++) {
		for (int x = 0; x < 800; x++) {
			const int pixel_i = pixel_index (x, y) * 4;
			
			const float xf = (x - 400) * scale;
			const float yf = (y - 240) * scale;
			
			const vec3 start = camera_pos + xf * camera_right + yf * camera_up;
			
			const auto trace_result = raytrace (voxels, start, dir);
			
			if (trace_result.hit) {
				pixels [pixel_i + 0] = trace_result.z;
				pixels [pixel_i + 1] = trace_result.y;
				pixels [pixel_i + 2] = trace_result.x;
			}
			else {
				pixels [pixel_i + 0] = 80;
				pixels [pixel_i + 1] = 58;
				pixels [pixel_i + 2] = 58;
			}
		}
	}
}

int main () {
	SdlRaii sdl (800, 480);
	vector <uint8_t> pixels;
	pixels.resize (800 * 480 * 4);
	
	vector <bool> voxels;
	const int res = res_at_level (max_level);
	voxels.resize (res * res * res);
	
	make_voxels (voxels);
	const auto leaves = make_leaves (voxels);
	auto unique_leaves = leaves;
	
	sort (unique_leaves.begin (), unique_leaves.end ());
	
	int unique_leaf_count = 0;
	for (int i = 1; i < unique_leaves.size (); i++) {
		if (unique_leaves [i] != unique_leaves [i - 1]) {
			unique_leaf_count++;
		}
	}
	
	cout << "Leaves: " << leaves.size () << endl;
	cout << "De-duped leaves: " << unique_leaf_count << endl;
	
	render_voxels_ortho (leaves, pixels.data ());
	
	SDL_UpdateTexture (sdl.texture, nullptr, pixels.data (), 800 * 4);
	SDL_RenderCopy (sdl.renderer, sdl.texture, nullptr, nullptr);
	SDL_RenderPresent (sdl.renderer);
	
	bool running = true;
	while (running) {
		SDL_Event event;
		while (SDL_PollEvent (&event)) {
			switch (event.type) {
				case SDL_QUIT:
				case SDL_KEYDOWN:
					running = false;
					break;
				default:
					break;
			}
		}
		
		SDL_Delay (2);
		SDL_RenderCopy (sdl.renderer, sdl.texture, nullptr, nullptr);
		SDL_RenderPresent (sdl.renderer);
	}
	
	return 0;
}
